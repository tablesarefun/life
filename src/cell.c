#include "cell.h"
#include "vectors.h"
#include "random.h"

struct cell make_cell(struct vec2 pos, enum cell_type type)
{
	/* TODO: This is just for testing purposes. Have a different way
     * of figuring out the ticks to live. */
	size_t ttl = random_uint(20, 50);
	return (struct cell){ pos, ttl, type };
}

struct vec2 random_walk(struct cell *cell, size_t width, size_t height)
{
	int dx;
	int dy;

	do
	{
		dx = random_uint(0, 2) - 1;
		dy = random_uint(0, 2) - 1;
	} while (cell->pos.x + dx < 0 || cell->pos.x + dx >= width ||
	         cell->pos.y + dy < 0 || cell->pos.y + dy >= height);

	struct vec2 dv = (struct vec2){ dx, dy };
	cell->pos = vec2_add(cell->pos, dv);

	return dv;
}
