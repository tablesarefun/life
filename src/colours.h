#ifndef COLOURS_H_
#define COLOURS_H_

/* Colours */
#define BLACK (ALLEGRO_COLOR){ 0.0f, 0.0f, 0.0f, 255.0f }
#define BACKGROUND_WHITE (ALLEGRO_COLOR){ 245.0f, 245.0f, 245.0f, 255.0f }

/* Cell colours */
#define CELL_ALIVE_COLOUR (ALLEGRO_COLOR){ 255.0f, 0.0f, 0.0f, 255.0f }

#endif /* COLOURS_H_ */
