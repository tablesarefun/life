#include <stdlib.h>
#include <time.h>
#include "random.h"

/* Seeds rand() using srand() with time(NULL) */
/* TODO: Maybe there is a better way to seed the rand() function */
int new_seed(void)
{
	int seed = time(NULL);
	srand(seed);

	return seed;
}

unsigned random_uint(unsigned min, unsigned max)
{
	return (rand() % (max + 1)) + min;
}
