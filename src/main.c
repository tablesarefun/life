#include <allegro5/allegro.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "world.h"
#include "cell.h"
#include "colours.h"

#define FPS 30.0f
#define TPS 10.0f /* Ticks per second, each tick the simulation is advanced by 1 */

#define WINDOW_WIDTH 600
#define WINDOW_HEIGHT 600

#define CELL_SIZE 4 /* width and height of a cell in pixels */
#define WORLD_WIDTH WINDOW_WIDTH / CELL_SIZE
#define WORLD_HEIGHT WINDOW_HEIGHT / CELL_SIZE
#define NUM_CELLS WORLD_WIDTH * WORLD_HEIGHT
#define INITIAL_POPULATION_FRACTION 100

enum KEY_STATE
{
	KEY_SEEN = 1,
	KEY_RELEASED,
};

static unsigned char keys[ALLEGRO_KEY_MAX];

void must_init(bool test, const char *msg)
{
	if (test)
		return;

	fprintf(stderr, "Unable to initialise %s.\n", msg);
	exit(1);
}

int main(void)
{
	/* Initialising */
	must_init(al_init(), "allegro");
	must_init(al_install_keyboard(), "keyboard");

	ALLEGRO_TIMER *timer = al_create_timer(1.0 / FPS);
	must_init(timer, "timer");

	ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
	must_init(queue, "event queue");

	ALLEGRO_DISPLAY *disp = al_create_display(WINDOW_WIDTH, WINDOW_HEIGHT);
	must_init(disp, "display");

	al_register_event_source(queue, al_get_keyboard_event_source());
	al_register_event_source(queue, al_get_display_event_source(disp));
	al_register_event_source(queue, al_get_timer_event_source(timer));

	/* Loop */
	memset(keys, 0, sizeof(keys));

	ALLEGRO_EVENT event;
	bool is_done = false;
	bool redraw = true;
	bool paused = false;

	double total_time = 0.0f;
	double dt_accumulator = 0.0f;
	double time = 0.0f;
	double dt = 0.0f;

	struct world world = { 0 };
	world = initialise_world(WORLD_WIDTH, WORLD_HEIGHT, CELL_SIZE, 100);
	populate_world(&world, INITIAL_POPULATION_FRACTION);

	printf("cell count: %zu\n", world.length);
	printf("world width: %zu\tworld height: %zu\n", WORLD_WIDTH, WORLD_HEIGHT);

	al_start_timer(timer);
	while (true)
	{
		al_wait_for_event(queue, &event);

		double new_time = al_get_time();
		dt = new_time - time;
		time = new_time;
		dt_accumulator += dt;

		/* Update */
		switch (event.type)
		{
		case ALLEGRO_EVENT_TIMER:
			if (!paused)
			{

				while (dt_accumulator >= 1.0 / TPS)
				{
					update_world(&world);

					dt_accumulator -= 1.0 / TPS;
					total_time += 1.0 / TPS;
				}
			}

			if (keys[ALLEGRO_KEY_ESCAPE])
			{
				is_done = true;
			}

			for (size_t i = 0; i < ALLEGRO_KEY_MAX; ++i)
			{
				keys[i] &= KEY_SEEN;
			}

			redraw = true;
		break;
		case ALLEGRO_EVENT_KEY_DOWN:
			keys[event.keyboard.keycode] = KEY_SEEN | KEY_RELEASED;

			if (event.keyboard.keycode == ALLEGRO_KEY_SPACE)
			{
				paused = !paused;
			}
		break;
		case ALLEGRO_EVENT_KEY_UP:
			keys[event.keyboard.keycode] &= KEY_RELEASED;
		break;
		case ALLEGRO_EVENT_DISPLAY_CLOSE:
			is_done = true;
		break;
		}

		if (is_done)
		{
			break;
		}

		/* Draw */
		if (redraw && al_is_event_queue_empty(queue))
		{
			al_clear_to_color(BACKGROUND_WHITE);

			draw_world(world);

			al_flip_display();
			redraw = false;
		}
	}

	al_destroy_display(disp);
	al_destroy_event_queue(queue);
	al_destroy_timer(timer);
	destroy_world(world);

	printf("uptime: %g seconds\n", total_time);

	return 0;
}
