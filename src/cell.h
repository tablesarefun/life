#ifndef CELL_H_
#define CELL_H_

#include <stddef.h>
#include "vectors.h"

/* Maybe have cell states too. Different types can have different states etc. */
enum cell_type
{
	CELL_ALIVE = 0,
	NUM_CELL_TYPES,
};

struct cell
{
	struct vec2 pos;
	size_t ticks_to_live;
	enum cell_type type;
};

/* TOOD: Maybe have the ticks_to_live in here? */
struct cell make_cell(struct vec2 pos, enum cell_type type);
struct vec2 random_walk(struct cell *cell, size_t width, size_t height);

#endif /* CELL_H_ */
