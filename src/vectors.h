#ifndef VECTORS_H_
#define VECTORS_H_

struct vec2
{
	int x;
	int y;
};

struct vec2 vec2_add(struct vec2 v, struct vec2 u);

#endif /* VECTORS_H_ */
