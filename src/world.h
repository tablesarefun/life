#ifndef WORLD_H_
#define WORLD_H_

#include "cell.h"

struct world
{
	/* TODO: Maybe move this out into its own struct? */
	struct
	{
		struct cell *cells;
		size_t length;
		size_t size;
	};

	size_t cell_size;
	size_t width;
	size_t height;
};

struct world initialise_world(size_t width, size_t height, size_t cell_size, size_t initial_world_size);
/* Populates world with an alive fill percentage of 1/n */
void populate_world(struct world *world, int n);
void add_cell_to_world(struct world *world, struct cell cell);
void increase_world_size(struct world *world);
void kill_cell(struct world *world, size_t cell_index);
void update_world(struct world *world);
void draw_world(struct world world);
void destroy_world(struct world);

#endif /* WORLD_H_ */
