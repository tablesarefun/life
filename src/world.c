#include <allegro5/allegro_primitives.h>
#include <stdlib.h>
#include "world.h"
#include "cell.h"
#include "colours.h"
#include "random.h"

/* Maybe have the world be a static variable in this file rather than passing it around? */
struct world initialise_world(size_t width, size_t height, size_t cell_size, size_t initial_world_size)
{
	struct world world = { 0 };

	world.cells = malloc(sizeof(*world.cells) * initial_world_size);
	world.length = 0;
	world.size = initial_world_size;
	world.cell_size = cell_size;
	world.width = width;
	world.height = height;

	return world;
}

/* Populates world with an alive fill percentage of 1/n */
/* TODO: The fill percentage should be changed or made more readable */
void populate_world(struct world *world, int n)
{
	/* new_seed(); */
	for (size_t i = 0; i < world->width * world->height; ++i)
	{
		int x = i % world->width;
		int y = i / world->width;

		unsigned p = random_uint(0, n - 1);

		if (!p)
		{
			struct cell cell = make_cell((struct vec2){ x, y }, CELL_ALIVE);
			/* TODO: Make this a little more readable, maybe by having a create_cell() function */
			add_cell_to_world(world, cell);
		}
	}
}

void add_cell_to_world(struct world *world, struct cell cell)
{
	if (world->length == world->size)
	{
		increase_world_size(world);
	}

	world->cells[world->length] = cell;
	++world->length;
}

void increase_world_size(struct world *world)
{
	/* TODO: Check if realloc returns NULL */
	world->cells = realloc(world->cells, sizeof(*world->cells) * 2 * world->size);
	world->size = 2 * world->size;
}

void kill_cell(struct world *world, size_t cell_index)
{
	for (size_t i = cell_index; i < world->length; ++i)
	{
		world->cells[i] = world->cells[i+1];
	}

	--world->length;
}

void update_world(struct world *world)
{
	for (size_t i = 0; i < world->length; ++i)
	{
		random_walk(&world->cells[i], world->width, world->height);
		--world->cells[i].ticks_to_live;

		if (world->cells[i].ticks_to_live == 0)
		{
			kill_cell(world, i);
		}
	}
}

void draw_world(struct world world)
{
	for (size_t i = 0; i < world.length; ++i)
	{
		float x1 = (float)(world.cells[i].pos.x) * world.cell_size; 
		float y1 = (float)(world.cells[i].pos.y) * world.cell_size;
		float x2 = x1 + world.cell_size;
		float y2 = y1 + world.cell_size;

		al_draw_filled_rectangle(x1, y1, x2, y2, CELL_ALIVE_COLOUR);
	}
}

void destroy_world(struct world world)
{
	if (world.cells)
	{
		free(world.cells);
	}
}
