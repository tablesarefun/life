#include "vectors.h"

struct vec2 vec2_add(struct vec2 v, struct vec2 u)
{
	return (struct vec2){ v.x + u.x, v.y + u.y };
}
