#ifndef RANDOM_H_
#define RANDOM_H_

/* Seeds rand() using srand() with time(NULL) */
/* TODO: Maybe there is a better way to seed the rand() function */
int new_seed(void);
unsigned random_uint(unsigned min, unsigned max);

#endif /* RANDOM_H_ */
